//====================================================
//  Texture Mapping
//  Created by Viet Trinh on 02/20/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//====================================================

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include "readBMP.h"

#define PI 3.14159265359
using namespace std;

/*========= CLASSES DECLARATION =============*/
//----- class Point -----//
class Point{
    float x;
    float z;
    float h;
public:
    Point(){x=0;z=0;h=0;}
    Point(float xp,float zp){x = xp;z=zp;h=0;}
    Point(float xp,float zp,float hp){x = xp;z=zp;h=hp;}
    void setCoord(float xp,float zp){x = xp;z=zp;}
    void setHeight(float hp){h = hp;}
    float getXCoord(){return x;}
    float getZCoord(){return z;}
    float getHeight(){return h;}
};

//----- class Vector -----//
class Vector{
public:
    float x;
    float y;
    float z;
    
    Vector(){x = 0.0;y=0.0;z=0.0;}
};

/*========= GLOBAL VARIABLES =========*/
//--- Scene ----//
int button, state = 1;
float gx, gy;
float x_win =800.0;
float z_win = 600.0;
float obj_material_amb[4];
float obj_material_diff[4];
float obj_material_spec[4];
float obj_light_ambient[4] = { 0.2, 0.2, 0.2, 1.0 };     // r, g, b, a
float obj_light_diffuse[4] = { 0.8, 0.3, 0.1, 1.0 };     // r, g, b, a
float obj_light_specular[4] = { 0.8, 0.3, 0.1, 1.0 };    // r, g, b, a
float LIGHT_POS[4] = {0.0, 200.0, 500.0 , 0.0};   // x, y, z, w
float VIEWER[3]={0.0,150.0,20.0};
float LOOKAT[3]={static_cast<float>(x_win/2.0),0.0,static_cast<float>(z_win/2.0)};

//--- Terrain ----//
Vector direction;           // direction vector from viewer to look-at-point
Point* vertices[153][153];  // Arrays of vertices for each sub-regions of the terrain
float smoothVertices[153][153];
float ROUGHNESS = 0.1;
int   steps = 152;
int smoothTimes = 40;

//--- Texture ----//
GLuint TextureArray[2];        // 0 is the texture for a terrain, 1 is the other texture
Image skyTexture;
Image terrainTexture;
float grid[153][153][3];
float tur_s;
int tur_N,tur_b,tur_a;

//---- Shadow ----//
bool inLightPerspective = true;
double Vmodelview[16],Vprojection[16],Lmodelview[16],Lprojection[16];
int Vviewport[4],Lviewport[4];
float Ldepth[800*600],Vdepth[800*600],Vcolor[800*600*3];

/*========= FUNCTIONS DECLARATION =========*/
//----- Screen functions -----//
void screenSetUp_ViewerPerpective();
void screenSetUp_LightPerpective();
void displayScreen();
void mouseClicks(int but,int sta,int x,int y);
void mouseMoves(int x, int y);
void keyPresses(unsigned char c, int x, int y);
void RotateLeftRight(float angle);
void MoveUpDown(float coef);

//----- Terrain functions -----//
void initialData();
float Gaussian();
float MidPointDisplacement(float yb,float ya,float b,float a);
bool setTerrainData(int x0,int y0,int x1,int y1);
void smoothTerrain();
Vector* calculateNormal(Point* a, Point* b, Point* c);
void drawRegion(int y0,int x0,int y1,int x1);
void DrawTerrain();
void drawTree(float r, float h, int level);
void DrawSky();
void drawScene();

//----- Texture functions -----//
Vector* Interpolating(float dx,float dz,int x0,int z0,int x1,int z1);
Vector* Noise(float xp,float zp);
Vector* Turbulence(float s, float xp,float zp);
void MakeTerrainTexture();
void MakeSkyTexture();
void TestTexture();

//----- Shadow functions -----//
void getMatrices_ViewerPerspective();
void getMatrices_LightPerspective();
void buildShadowMap();

//===== main function ==================================
int main(int argc, char**argv){
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	
	// Create a window
	glutCreateWindow("Texture Mapping");
	glutPositionWindow(20, 20);
	glutReshapeWindow(x_win, z_win);
	
	// Program start here...
    initialData();
	glutDisplayFunc(displayScreen);
	glutMouseFunc(mouseClicks);
	glutMotionFunc(mouseMoves);
    glutKeyboardFunc(keyPresses);
	glutMainLoop();
	return 0;
}

//==================================================
void initialData(){
    cout<< "Setting up terrain vertices..."<<endl;
    // setup vertices terrain
    for(int i=0;i<(steps+1);i++){
        for(int j=0;j<(steps+1);j++){
            vertices[i][j] = NULL;
        }
    }
    
    // setup 4 corners of the terrain
    vertices[0][0]   = new Point(0.0, 0.0, 0.0);
    vertices[steps][0]  = new Point(x_win, 0.0, 60.0);
    vertices[steps][steps] = new Point(x_win, z_win, 100.0);
    vertices[0][steps]  = new Point(0.0, z_win, 10.0);
    
    // generate and smoothen vertices of the terrain
    setTerrainData(0, 0, steps, steps);
    for(int i=0;i<smoothTimes;i++)
        smoothTerrain();
    
    cout<< "Setting up 2D terrain texture grid"<<endl;
    // initialize grid for trilinear interpolation
    for(int i=0;i<153;i++){
        for (int j=0; j<153; j++) {
            grid[i][j][0] = (rand()%(240-0+1))+0;
            grid[i][j][1] = (rand()%(255-139+1))+139;
            grid[i][j][2] = (rand()%(154-0+1))+0;
        }
    }
    
    // Turbulence coefficieny
    tur_N = 5;
    tur_a = 2;
    tur_b = 2;
    tur_s = 2;
    
    // Create 256x256 Textures
    terrainTexture.sizeX = 256;
    terrainTexture.sizeY = 256;
    terrainTexture.data = (char *) malloc(256*256*3);
    
    cout<< "Creating textures..."<<endl;
    glGenTextures(2, TextureArray);
    MakeTerrainTexture();
    MakeSkyTexture();
    
}

//===== screenSetUp_ViewerPerpective ====================================
void screenSetUp_ViewerPerpective(){
    
    glClearColor(0.0,0.0,0.0,1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    /*---- Projection setup ----*/
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,x_win/z_win,5.0,5000.0);
    
    /*---- Modelview setup ----*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    /*---- Viewport setup ----*/
    glViewport(0,0,x_win,z_win);
    
    /*---- lighting setup -----*/
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_POINT_SMOOTH);
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, obj_light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, obj_light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, obj_light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POS);
    glEnable(GL_LIGHT0);
    
    gluLookAt(VIEWER[0],VIEWER[1],VIEWER[2],LOOKAT[0],LOOKAT[1],LOOKAT[2],0,1,0); // upvector is y-axis
}

//===== screenSetUp_LightPerpective ====================================
void screenSetUp_LightPerpective(){
    
    glClearColor(0.0,0.0,0.0,1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    /*---- Projection setup ----*/
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,x_win/z_win,5.0,5000.0);
    
    /*---- Modelview setup ----*/
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    /*---- Viewport setup ----*/
    glViewport(0,0,x_win,z_win);
    
    /*---- lighting setup -----*/
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_POINT_SMOOTH);
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, obj_light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, obj_light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, obj_light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POS);
    glEnable(GL_LIGHT0);
    
    gluLookAt(LIGHT_POS[0],LIGHT_POS[1],LIGHT_POS[2],LOOKAT[0],LOOKAT[1],LOOKAT[2],0,1,0); // upvector is y-axis
}

//===== displayScreen ===================================
void displayScreen(){
    
    /*----- Render scene in light perspective -----*/
    screenSetUp_LightPerpective();
    drawScene();
    getMatrices_LightPerspective();
    glReadPixels(0, 0, x_win, z_win, GL_DEPTH_COMPONENT, GL_FLOAT, Ldepth);
    cout<<"Finished rendering scene in light perspective"<<endl;
    
    /*----- Render scene in viewer perspective -----*/
    inLightPerspective = false;
    screenSetUp_ViewerPerpective();
    drawScene();
    getMatrices_ViewerPerspective();
    glReadPixels(0, 0, x_win, z_win, GL_DEPTH_COMPONENT, GL_FLOAT, Vdepth);
    glReadPixels(0, 0, x_win, z_win, GL_RGB,GL_FLOAT, Vcolor);
    cout<<"Finished rendering scene in viewer perspective"<<endl;
    
    /*----- Updating color and depth buffers -----*/
    buildShadowMap();
    glDrawPixels(x_win, z_win, GL_RGB, GL_FLOAT, Vcolor);
    cout<<"Finished updating frame buffers"<<endl;
    
    glutSwapBuffers();
}

//===== mouseClicks ====================================
void mouseClicks(int but,int sta,int x,int y){
    button = but;
    state = sta;
    
    
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		
        gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here...
    }
    
    glutPostRedisplay();
    
}

//===== mouseMoves ====================================
void mouseMoves(int x, int y){
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here..
        
    }
    
    glutPostRedisplay();
    
}

//===== keyPresses ====================================
void keyPresses(unsigned char c, int x, int y){
    
    if (c =='w'){ MoveUpDown(10.0);}
    else if (c =='x'){ MoveUpDown(-10.0);}
    else if (c =='a'){ RotateLeftRight(10*PI/180.0);}
    else if (c =='d'){ RotateLeftRight(-10*PI/180.0);}
    else if (c =='s'){ smoothTerrain();}
    else {;}
    
    glutPostRedisplay();
}

//=================================================================
void MoveUpDown(float coef){
    
    float distance;
    
    // normalize direction vector
    direction.x = LOOKAT[0] - VIEWER[0];
    direction.y = LOOKAT[1] - VIEWER[1];
    direction.z = LOOKAT[2] - VIEWER[2];
    
    distance = sqrt(direction.x*direction.x + direction.y*direction.y + direction.z*direction.z);
    
    direction.x = direction.x/distance;
    direction.y = direction.y/distance;
    direction.z = direction.z/distance;
    
    // translate viewer and look-at-point positions
    VIEWER[0] += coef*direction.x;
    VIEWER[2] += coef*direction.z;
    LOOKAT[0] += coef*direction.x;
    LOOKAT[2] += coef*direction.z;
    
}

//=================================================================
void RotateLeftRight(float angle){
    float x1,y1,z1,x2,y2,z2;
    
    // translate to origin
    x1 = LOOKAT[0] - VIEWER[0];
    y1 = LOOKAT[1] - VIEWER[1];
    z1 = LOOKAT[2] - VIEWER[2];
    
    // rotate around a pivot
    x2 = x1*cos(angle) + z1*sin(angle);
    y2 = y1;
    z2 = -x1*sin(angle) + z1*cos(angle);
    
    // translate back
    LOOKAT[0] = x2 + VIEWER[0];
    LOOKAT[1] = y2 + VIEWER[1];
    LOOKAT[2] = z2 + VIEWER[2];
}

//=================================================================
void drawScene(){
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    DrawTerrain();
    glPushMatrix();
    glTranslatef(200.0, 5.0, 350.0);
    cout<< "Terrain: draw done"<<endl;
    drawTree(4.0, 55.0, 7);
    cout<< "Tree: draw done"<<endl;
    glPopMatrix();
    if(!inLightPerspective){
        DrawSky();
        cout<< "Sky: draw done"<<endl;
    }
}

//=================================================================
void getMatrices_ViewerPerspective(){
    
    glGetDoublev(GL_PROJECTION_MATRIX, Vprojection);
    glGetDoublev(GL_MODELVIEW_MATRIX, Vmodelview);
    glGetIntegerv(GL_VIEWPORT,  Vviewport);
}

//=================================================================
void getMatrices_LightPerspective(){
    
    glGetDoublev(GL_PROJECTION_MATRIX, Lprojection);
    glGetDoublev(GL_MODELVIEW_MATRIX, Lmodelview);
    glGetIntegerv(GL_VIEWPORT,  Lviewport);
}

//=================================================================
void buildShadowMap(){
    
    GLdouble objx,objy,objz;
    GLdouble winx,winy,winz;
    GLint ret;
    
    for (int i=0; i<x_win; i++) {
        for (int j=0; j<z_win; j++) {
            gluUnProject(i, j, Vdepth[i+j*((int)x_win)], Vmodelview, Vprojection, Vviewport, &objx, &objy, &objz);
            ret = gluProject(objx, objy, objz, Lmodelview, Lprojection, Lviewport, &winx, &winy, &winz);
            
            if(ret==GLU_FALSE){ cout << "Project to Light Perspective: FAILED"<<endl;}
            
            int px = winx;
            int py = winy;
            
            if(px>=0 && px<x_win && py>=0 && py<z_win){
                float d =Ldepth[px+py*((int)x_win)];
                float wz = winz- (winz*100 - ((int)(winz*100)))/100;
                if((wz-d) > 0.001){
                    Vcolor[(i+j*((int)x_win))*3+0] = .1;
                    Vcolor[(i+j*((int)x_win))*3+1] = .1;
                    Vcolor[(i+j*((int)x_win))*3+2] = .1;
                }
            }
        }
    }
}

//=================================================================
float Gaussian(){
    
    static double V1, V2, S;
	static int phase = 0;
	double X;
    
	if(phase == 0) {
		do {
            double U1 = (double)rand() / RAND_MAX;
			double U2 = (double)rand() / RAND_MAX;
			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
        } while(S >= 1 || S == 0);
        
		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);
    
	phase = 1 - phase;
	return X;
}

//=================================================================
float MidPointDisplacement(float yb,float ya,float b,float a){
    float mid,g,r;
    
    g = Gaussian();
    r = ROUGHNESS*g*(abs(b-a));
    mid = (ya+yb)/2.0 + r;
    
    return mid;
}

//=================================================================
Vector* calculateNormal(Point* a, Point* b, Point* c){
    Vector* ret = new Vector();
    Vector v1,v2,v;
    float d;
    
    // Vector ab
    v1.x = b->getXCoord() - a->getXCoord();
    v1.y = b->getHeight() - a->getHeight();
    v1.z = b->getZCoord() - a->getZCoord();
    
    // Vector ac
    v2.x = c->getXCoord() - b->getXCoord();
    v2.y = c->getHeight() - b->getHeight();
    v2.z = c->getZCoord() - b->getZCoord();
    
    // Vector ab x ac
    v.x = v1.y*v2.z - v2.y*v1.z;
    v.y = v1.z*v2.x - v2.z*v1.x;
    v.z = v1.x*v2.y - v2.x*v1.y;
    
    d = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
    
    // Normalize vector ab x bc
    ret->x = v.x/d;
    ret->y = v.y/d;
    ret->z = v.z/d;
    
    return ret;
}

//=================================================================
bool setTerrainData(int x0,int y0,int x1,int y1){
    int midX,midY;
    
    if((x1-x0)<2 && (y1-y0)<2){
        return false;
    }
    else{
        midX = (x0+x1)/2;
        midY = (y0+y1)/2;
        // calculate the height of the mid_bottom
        if (vertices[midX][y0] == NULL){
            vertices[midX][y0] = new Point((vertices[x1][y0]->getXCoord() + vertices[x0][y0]->getXCoord())/2.0,
                                           (vertices[x1][y0]->getZCoord() + vertices[x0][y0]->getZCoord())/2.0,
                                           MidPointDisplacement(vertices[x1][y0]->getHeight(), vertices[x0][y0]->getHeight(),
                                                                vertices[x1][y0]->getXCoord(), vertices[x0][y0]->getXCoord()));
        }
        
        // calculate the height of the mid_top
        if (vertices[midX][y1] == NULL){
            vertices[midX][y1] = new Point((vertices[x1][y1]->getXCoord() + vertices[x0][y1]->getXCoord())/2.0,
                                           (vertices[x1][y1]->getZCoord() + vertices[x0][y1]->getZCoord())/2.0,
                                           MidPointDisplacement(vertices[x1][y1]->getHeight(), vertices[x0][y1]->getHeight(),
                                                                vertices[x1][y1]->getXCoord(), vertices[x0][y1]->getXCoord()));
        }
        
        // calculate the height of the mid_left
        if (vertices[x0][midY] == NULL){
            vertices[x0][midY] = new Point((vertices[x0][y1]->getXCoord() + vertices[x0][y0]->getXCoord())/2.0,
                                           (vertices[x0][y1]->getZCoord() + vertices[x0][y0]->getZCoord())/2.0,
                                           MidPointDisplacement(vertices[x0][y1]->getHeight(), vertices[x0][y0]->getHeight(),
                                                                vertices[x0][y1]->getZCoord(), vertices[x0][y0]->getZCoord()));
        }
        // calculate the height of the mid_right
        if (vertices[x1][midY] == NULL){
            vertices[x1][midY] = new Point((vertices[x1][y1]->getXCoord() + vertices[x1][y0]->getXCoord())/2.0,
                                           (vertices[x1][y1]->getZCoord() + vertices[x1][y0]->getZCoord())/2.0,
                                           MidPointDisplacement(vertices[x1][y1]->getHeight(), vertices[x1][y0]->getHeight(),
                                                                vertices[x1][y1]->getZCoord(), vertices[x1][y0]->getZCoord()));
        }
        
        // calculate the height of the mid point
        if (vertices[midX][midY] == NULL){
            vertices[midX][midY] = new Point((vertices[x1][y1]->getXCoord() + vertices[x0][y0]->getXCoord())/2.0,
                                             (vertices[x1][y1]->getZCoord() + vertices[x0][y0]->getZCoord())/2.0,
                                             MidPointDisplacement(vertices[x1][y1]->getHeight(), vertices[x0][y0]->getHeight(),
                                                                  vertices[x1][y1]->getXCoord(), vertices[x0][y0]->getXCoord()));
        }
        
        // recursively to all quadrants
        setTerrainData(x0, y0, midX, midY);
        setTerrainData(midX, y0, x1, midY);
        setTerrainData(x0, midY, midX, y1);
        setTerrainData(midX, midY, x1, y1);
        
        return true;
    }
}

//=================================================================
void DrawTerrain(){
    
    // set material color for the terrain
    obj_material_amb[0] = 1.0;
    obj_material_amb[1] = 1.0;
    obj_material_amb[2] = 1.0;
    obj_material_amb[3] = 1.0;
    
    obj_material_diff[0] = 1.0;
    obj_material_diff[1] = 1.0;
    obj_material_diff[2] = 1.0;
    obj_material_diff[3] = 1.0;
    
    obj_material_spec[0] = 0.0;
    obj_material_spec[1] = 0.0;
    obj_material_spec[2] = 0.0;
    obj_material_spec[3] = 1.0;
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, obj_material_diff);
    glMaterialfv(GL_FRONT, GL_SPECULAR, obj_material_spec);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, TextureArray[0]);
    //cout<<"texture ID1= "<<TextureArray[0]<<endl;
    
    /* draw sub-regions in the terrain */
    for (int y=0; y<steps; y++) {
        for (int x=0; x<steps; x++) {
            drawRegion(y, x, y+1, x+1);
        }
    }
    glDisable(GL_TEXTURE_2D);
    
    /*====== Test Texture =========
     glPushMatrix();
     glTranslatef(10.0, 10.0, 30.0);
     glRotatef(45, 0.0, 1.0, .0);
     glScalef(2.0, 2.0, 2.0);
     TestTexture();
     glPopMatrix();*/
}

//=================================================================
void drawRegion(int y0,int x0,int y1,int x1){
    Vector* norm;
    
    // draw bottom triangle
    norm = calculateNormal(vertices[x0][y0],vertices[x1][y1],vertices[x1][y0]);
    glBegin(GL_POLYGON);
    glNormal3f(norm->x, norm->y, norm->z);
    glTexCoord2f((vertices[x0][y0]->getXCoord())/800.0*2.0, (vertices[x0][y0]->getZCoord())/600.0*2.0);
    glVertex3f(vertices[x0][y0]->getXCoord(), vertices[x0][y0]->getHeight(), vertices[x0][y0]->getZCoord());
    glTexCoord2f((vertices[x1][y0]->getXCoord())/800.0*2.0,  (vertices[x1][y0]->getZCoord())/600.0*2.0);
    glVertex3f(vertices[x1][y0]->getXCoord(), vertices[x1][y0]->getHeight(), vertices[x1][y0]->getZCoord());
    glTexCoord2f((vertices[x1][y1]->getXCoord())/800.0*2.0,  (vertices[x1][y1]->getZCoord())/600.0*2.0);
    glVertex3f(vertices[x1][y1]->getXCoord(), vertices[x1][y1]->getHeight(), vertices[x1][y1]->getZCoord());
    glEnd();
    
    // draw top triangle
    norm = calculateNormal(vertices[x0][y0],vertices[x0][y1],vertices[x1][y1]);
    glBegin(GL_POLYGON);
    
    glNormal3f(norm->x, norm->y, norm->z);
    glTexCoord2f((vertices[x0][y0]->getXCoord())/800.0*2.0, (vertices[x0][y0]->getZCoord())/600.0*2.0);
    glVertex3f(vertices[x0][y0]->getXCoord(), vertices[x0][y0]->getHeight(), vertices[x0][y0]->getZCoord());
    glTexCoord2f((vertices[x1][y1]->getXCoord())/800.0*2.0,  (vertices[x1][y1]->getZCoord())/600.0*2.0);
    glVertex3f(vertices[x1][y1]->getXCoord(), vertices[x1][y1]->getHeight(), vertices[x1][y1]->getZCoord());
    glTexCoord2f((vertices[x0][y1]->getXCoord())/800.0*2.0,  (vertices[x0][y1]->getZCoord())/600.0*2.0);
    glVertex3f(vertices[x0][y1]->getXCoord(), vertices[x0][y1]->getHeight(), vertices[x0][y1]->getZCoord());
    glEnd();
    
}

//=================================================================
void smoothTerrain(){
    
    //calculate the new smooth heights for each vertex in the terrain
    for(int r=0;r<=steps;r++){
        for (int c=0; c<=steps; c++) {
            
            /*----- bottome row -----*/
            if (r==0){
                if(c==0) {smoothVertices[r][c] = (1.0/3.0)*(vertices[r][c+1]->getHeight()+
                                                            vertices[r+1][c]->getHeight()+
                                                            vertices[r+1][c+1]->getHeight());}
                
                else if (c==steps){smoothVertices[r][c] = (1.0/3.0)*(vertices[r][c-1]->getHeight()+
                                                                     vertices[r+1][c-1]->getHeight()+
                                                                     vertices[r+1][c]->getHeight());}
                
                else {smoothVertices[r][c] = (1.0/5.0)*(vertices[r][c-1]->getHeight()+
                                                        vertices[r][c+1]->getHeight()+
                                                        vertices[r+1][c-1]->getHeight()+
                                                        vertices[r+1][c]->getHeight()+
                                                        vertices[r+1][c+1]->getHeight());}
            }
            
            /*----- top row -----*/
            else if (r==steps){
                if(c==0) {smoothVertices[r][c] = (1.0/3.0)*(vertices[r][c+1]->getHeight()+
                                                            vertices[r-1][c]->getHeight()+
                                                            vertices[r-1][c+1]->getHeight());}
                
                else if (c==steps){smoothVertices[r][c] = (1/3)*(vertices[r][c-1]->getHeight()+
                                                                 vertices[r-1][c-1]->getHeight()+
                                                                 vertices[r-1][c]->getHeight());}
                
                else {smoothVertices[r][c] = (1.0/5.0)*(vertices[r][c-1]->getHeight()+
                                                        vertices[r][c+1]->getHeight()+
                                                        vertices[r-1][c-1]->getHeight()+
                                                        vertices[r-1][c]->getHeight()+
                                                        vertices[r-1][c+1]->getHeight());}
            }
            
            /*----- other rows -----*/
            else{
                if(c==0) {smoothVertices[r][c] = (1.0/5.0)*(vertices[r-1][c]->getHeight()+
                                                            vertices[r-1][c+1]->getHeight()+
                                                            vertices[r][c+1]->getHeight()+
                                                            vertices[r+1][c+1]->getHeight()+
                                                            vertices[r+1][c]->getHeight());}
                
                else if (c==steps){smoothVertices[r][c] = (1.0/5.0)*(vertices[r-1][c]->getHeight()+
                                                                     vertices[r-1][c-1]->getHeight()+
                                                                     vertices[r][c-1]->getHeight()+
                                                                     vertices[r+1][c-1]->getHeight()+
                                                                     vertices[r+1][c]->getHeight());}
                
                else {smoothVertices[r][c] = (1.0/8.0)*(vertices[r-1][c-1]->getHeight()+
                                                        vertices[r-1][c]->getHeight()+
                                                        vertices[r-1][c+1]->getHeight()+
                                                        vertices[r][c-1]->getHeight()+
                                                        vertices[r][c+1]->getHeight()+
                                                        vertices[r+1][c-1]->getHeight()+
                                                        vertices[r+1][c]->getHeight()+
                                                        vertices[r+1][c+1]->getHeight());}
            }
            //cout<<"done row "<<r <<" col "<<c<<endl;
        }
    }
    
    //update the heights of all vertices with new calculated heights
    for(int i=0;i<=steps;i++){
        for (int j =0; j<=steps; j++) {
            vertices[i][j]->setHeight(smoothVertices[i][j]);
        }
    }
    
}

//=================================================================
void drawTree(float r, float h, int level){
    GLUquadricObj *tree;
    
    if(level<2){    // set up material for leaves
        glEnable(GL_BLEND);                 // enable GL_BLEND because GL_BLEND is disable in previous iterations
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        obj_material_amb[0] = 165.0/255.0 ;
        obj_material_amb[1] = 42.0/255.0 ;
        obj_material_amb[2] = 42.0/255.0;
        obj_material_amb[3] = 1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, obj_material_amb);
        
    }
    else{           // set up material for tree
        glDisable(GL_BLEND);    // disable GL_BLEND because of showing the body of the tree
        obj_material_amb[0] = 49.0/255.0;
        obj_material_amb[1] = 79.0/255.0;
        obj_material_amb[2] = 79.0/255.0;
        obj_material_amb[3] = 1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, obj_material_amb);
    }
    
    // draw tree body
    tree = gluNewQuadric();
    gluQuadricDrawStyle(tree, GLU_FILL);
    glPushMatrix();
    glRotatef(-90.0, 1.0, 0.0, 0.0);
    gluCylinder(tree, r, r, h, 10, 1);
    glPopMatrix();
    
    if(level>0){
        // 1st branch
        glPushMatrix();
        glTranslatef(0.0, h, 0.0);
        glRotatef(60, 1.0, 0.0, 0.0);
        glRotatef(-60, 0.0, 1.0, 0.0);
        drawTree(0.6*r, 0.6*h, level-1);
        glPopMatrix();
        
        // 2nd branch
        glPushMatrix();
        glTranslatef(0.0, h, 0.0);
        glRotatef(20, 1.0, 0.0, 0.0);
        glRotatef(-20, 0.0, 0.0, 1.0);
        drawTree(0.6*r, 0.8*h, level-1);
        glPopMatrix();
        
        // 3rd branch
        glPushMatrix();
        glTranslatef(0.0, h, 0.0);
        glRotatef(-20, 1.0, 0.0, 0.0);
        glRotatef(20, 0.0, 0.0, 1.0);
        drawTree(0.6*r, 0.8*h, level-1);
        glPopMatrix();
        
        // 4th branch
        glPushMatrix();
        glTranslatef(0.0, h, 0.0);
        glRotatef(-50, 1.0, 0.0, 0.0);
        glRotatef(50, 0.0, 1.0, 0.0);
        drawTree(0.6*r, 0.6*h, level-1);
        glPopMatrix();
    }
}

//=================================================================
void DrawSky(){
    
    // Make the color white to it shows the texture better
    obj_material_amb[0] = 1.0;
    obj_material_amb[1] = 1.0;
    obj_material_amb[2] = 1.0;
    obj_material_amb[3] = 1.0;
    
    obj_material_diff[0] = 1.0;
    obj_material_diff[1] = 1.0;
    obj_material_diff[2] = 1.0;
    obj_material_diff[3] = 1.0;
    
    obj_material_spec[0] = 0.0;
    obj_material_spec[1] = 0.0;
    obj_material_spec[2] = 0.0;
    obj_material_spec[3] = 1.0;
    
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, obj_material_diff);
    glMaterialfv(GL_FRONT, GL_SPECULAR, obj_material_spec);
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, TextureArray[1]);
    
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(0.0, -30.0, z_win);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(0.0, 600.0, z_win);
    glTexCoord2f(1.0, 1.0);
    glVertex3f(x_win, 600.0, z_win);
    glTexCoord2f(1.0, 0.0);
    glVertex3f(x_win, -30.0, z_win);
    glEnd();
    
    glBegin(GL_POLYGON);
    glTexCoord2f(1.0, 0.0);
    glVertex3f(x_win, -30.0, z_win);
    glTexCoord2f(1.0, 1.0);
    glVertex3f(x_win, 600.0, z_win);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(x_win, 600.0, 0.0);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(x_win, -30.0, 0.0);
    glEnd();
}

//=================================================================
Vector* Interpolating(float dx,float dz,int x0,int z0,int x1,int z1){
    Vector* ret = new Vector();
    
    float   Cx0z_R,Cx1z_R,Cxz_R,
    Cx0z_G,Cx1z_G,Cxz_G,
    Cx0z_B,Cx1z_B,Cxz_B;
    
    Cx0z_R = grid[x0][z0][0]*(1.0-dz) + grid[x0][z1][0]*dz;
    Cx1z_R = grid[x1][z0][0]*(1.0-dz) + grid[x1][z1][0]*dz;
    Cxz_R = Cx0z_R*(1-dx) + Cx1z_R*dx;
    
    Cx0z_G = grid[x0][z0][1]*(1.0-dz) + grid[x0][z1][1]*dz;
    Cx1z_G = grid[x1][z0][1]*(1.0-dz) + grid[x1][z1][1]*dz;
    Cxz_G = Cx0z_G*(1-dx) + Cx1z_G*dx;
    
    Cx0z_B = grid[x0][z0][2]*(1.0-dz) + grid[x0][z1][2]*dz;
    Cx1z_B = grid[x1][z0][2]*(1.0-dz) + grid[x1][z1][2]*dz;
    Cxz_B = Cx0z_B*(1-dx) + Cx1z_B*dx;
    
    ret->x = Cxz_R;
    ret->y = Cxz_G;
    ret->z = Cxz_B;
    
    return ret;
}


//=================================================================
Vector* Noise(float xp,float zp){
    Vector* ret= NULL;
    int x0,z0,x1,z1;
    float dx,dz;
    
    x0 = (int)xp;
    z0 = (int)zp;
    dx = xp - (float)x0;
    dz = zp - (float)z0;
    
    if(dx > 0.0) {x1=x0+1;}
    else         {x1=x0;}
    
    if(dz > 0.0) {z1=z0+1;}
    else         {z1=z0;}
    
    ret = Interpolating(dx, dz, x0, z0, x1, z1);
    
    return ret;
}

//=================================================================
Vector* Turbulence(float s, float xp,float zp){
    Vector* ret = new Vector();
    Vector* iter_noise;
    float powa,powb;
    
    ret->x=0;
    ret->y=0;
    ret->z=0;
    
    for (int i=0; i<tur_N; i++) {
        powa = pow(tur_a, i+1);
        powb = pow(tur_b, i);
        iter_noise = Noise(s*powb*xp, s*powb*zp);
        ret->x += iter_noise->x/powa;
        ret->y += iter_noise->y/powa;
        ret->z += iter_noise->z/powa;
    }
    
    return ret;
}

//=================================================================
void MakeTerrainTexture(){
    Vector* color;
    float xp,zp;
    float coef_trans;
    
    coef_trans = 152.0/(255.0*(tur_s*pow(tur_b, tur_N-1)));
    
    for(int i=0;i<256;i++){
        for(int j=0;j<256;j++){
            xp = (float)i*coef_trans;
            zp = (float)j*coef_trans;
            color = Turbulence(tur_s, xp, zp);
            
            terrainTexture.data[(i*256+j)*3+0] = color->x;
            terrainTexture.data[(i*256+j)*3+1] = color->y;
            terrainTexture.data[(i*256+j)*3+2] = color->z;
        }
    }
    
    glBindTexture(GL_TEXTURE_2D, TextureArray[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 256, 256, 0, GL_RGB, GL_UNSIGNED_BYTE, terrainTexture.data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    cout<< "Terrain texture is created"<<endl;
    
}

//=================================================================
void MakeSkyTexture(){
    
    int flag = ImageLoad("/Users/viettrinh/Documents/Porfolio_Website/P8_Texture_Mapping/texture-mapping/Texture Mapping/Texture Mapping/sky.bmp", &skyTexture);
    if (flag != 1) {
        printf("Trouble reading image\n");
    }
    
    glBindTexture(GL_TEXTURE_2D, TextureArray[1]);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,(GLsizei)skyTexture.sizeX,(GLsizei)skyTexture.sizeY,0,GL_RGB,GL_UNSIGNED_BYTE,skyTexture.data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    cout<< "Sky texture is created"<<endl;
}

//=================================================================
void TestTexture(){
    
    /* Create a cube to test the texture*/
    //===== Front =====
    glBegin(GL_QUADS);
    glTexCoord2f(0.0,0.0);
    glVertex3f(5,5,5);
    glTexCoord2f(1.0,0.0);
    glVertex3f(5,5,15);
    glTexCoord2f(1.0,1.0);
    glVertex3f(5,15,15);
    glTexCoord2f(0.0,1.0);
    glVertex3f(5,15,5);
    glEnd();
    
    //===== Back =====
    glBegin(GL_QUADS);
    glTexCoord2f(0.0,0.0);
    glVertex3f(15,5,5);
    glTexCoord2f(1.0,0.0);
    glVertex3f(15,5,15);
    glTexCoord2f(1.0,1.0);
    glVertex3f(15,15,15);
    glTexCoord2f(0.0,1.0);
    glVertex3f(15,15,5);
    glEnd();
    
    //===== Top =====
    glBegin(GL_QUADS);
    glTexCoord2f(0.0,0.0);
    glVertex3f(5,5,5);
    glTexCoord2f(1.0,0.0);
    glVertex3f(5,15,15);
    glTexCoord2f(1.0,1.0);
    glVertex3f(15,15,15);
    glTexCoord2f(0.0,1.0);
    glVertex3f(15,15,5);
    glEnd();
    
    //===== Bottom =====
    glBegin(GL_QUADS);
    glTexCoord2f(0.0,0.0);
    glVertex3f(5,5,5);
    glTexCoord2f(1.0,0.0);
    glVertex3f(5,5,15);
    glTexCoord2f(1.0,1.0);
    glVertex3f(15,5,15);
    glTexCoord2f(0.0,1.0);
    glVertex3f(15,5,5);
    glEnd();
    
    //===== Left =====
    glBegin(GL_QUADS);
    glTexCoord2f(0.0,0.0);
    glVertex3f(5,5,5);
    glTexCoord2f(1.0,0.0);
    glVertex3f(15,5,5);
    glTexCoord2f(1.0,0.5);
    glVertex3f(15,15,5);
    glTexCoord2f(0.0,1.0);
    glVertex3f(5,15,5);
    glEnd();
    
    //===== Right =====
    glBegin(GL_QUADS);
    glTexCoord2f(0.0,0.0);
    glVertex3f(5,5,15);
    glTexCoord2f(1.0,0.0);
    glVertex3f(15,5,15);
    glTexCoord2f(1.0,1.0);
    glVertex3f(15,15,15);
    glTexCoord2f(0.0,1.0);
    glVertex3f(5,15,15);
    glEnd();
    
}





